# passbolt-terminal

**passbolt-terminal** is a passbolt cli tool with fuzzy search :yum:, made with **fzf**. 

[[_TOC_]]

![passbolt-terminal-video](passbolt-terminal.gif)

## Requirements:

* a bash console
* The excellent [go-passbolt-cli](https://github.com/passbolt/go-passbolt-cli) from @Speatzle  
*  [fzf for fuzzy search](https://github.com/junegunn/fzf#installation)
* **xclip** to copy data to the clipboard with any Linux. MacOS users will have to use **pbcopy**, Windows WSL users can use [win32yank](https://github.com/equalsraf/win32yank) or `C:\Windows\System32\copy.exe`
* whiptail for passbolt resources creation
* pwgen to generate strong passwords
* a passbolt instance

The most important part is to ensure go-passbolt-cli is working ([click here for instructions](https://github.com/passbolt/go-passbolt-cli#getting-started)) and you are able to retrieve data from your passbolt instance eg.

```
# Replace id with a resource id :-)
passbolt get resource --id b9dcba41-5880-4a23-8a4c-3ac3564cfa18
```

## Usage

Once launched, you can use the fuzzy search to search your secrets. You have a small pane on the right to preview them.

Some shortcuts are available:

* ctrl + c to copy password to clipboard (depending of xclip on Linux, pbcopy on MacOs and win32yank on WSL)
* ctrl + u to copy username
* ctrl + l to copy URL
* ctrl + o to open a browser (edit the browser path in the script)
* ctrl + n to create a new resource
* ctrl + d to delete a resource
* ctrl + space to clear the search field
* Escape key to quit :slight_smile: 

![preview|690x169](upload://lv5rzf0AM1vwQLlpOhEQyTngk3w.jpeg)

## About fzf

If you want to know more about fzf and create your own scripts, have a look at these resources:

* https://www.freecodecamp.org/news/fzf-a-command-line-fuzzy-finder-missing-demo-a7de312403ff/
* https://github.com/junegunn/fzf/wiki/examples

Please enjoy!
